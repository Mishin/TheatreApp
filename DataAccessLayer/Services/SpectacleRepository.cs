﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.EF;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer.Services
{
    public class SpectacleRepository : ISpectacleRepository
    {
        private readonly TheatreAppContext mContext;

        public SpectacleRepository(TheatreAppContext context)
        {
            mContext = context;
        }

        public int GetTotalItemsCount()
        {
            return mContext.Spectacles.Count();
        }

        public IEnumerable<Spectacle> GetAll()
        {
            return mContext.Spectacles.OrderBy(a => a.Name);
        }

        public void Add(Spectacle spectacle)
        {
            spectacle.Id = Guid.NewGuid();
            mContext.Spectacles.Add(spectacle);
        }

        public void Update(Spectacle spectacle)
        {
            var dbEntity = mContext.Spectacles.Find(spectacle.Id);
            if (dbEntity != null)
                mContext.Entry(dbEntity).CurrentValues.SetValues(spectacle);
        }

        public void Remove(Guid id)
        {
            var dbEntity = mContext.Spectacles.Find(id);
            if (dbEntity != null)
                mContext.Spectacles.Remove(dbEntity);
        }

        public Spectacle GetById(Guid id)
        {
            return mContext.Spectacles.Find(id);
        }
    }
}
