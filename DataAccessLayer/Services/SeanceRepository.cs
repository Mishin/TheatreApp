﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.EF;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System.Data.Entity;
using System.Data.SqlClient;

namespace DataAccessLayer.Services
{
    public class SeanceRepository : ISeanceRepository
    {
        private readonly TheatreAppContext mContext;

        public SeanceRepository(TheatreAppContext context)
        {
            mContext = context;
        }

        public IEnumerable<Seance> GetSeancesBySpectacle(Guid spectacleId)
        {
            return mContext.Seances.
                Include(a => a.Spectacle).
                Include(a => a.Reservations).
                Include(a => a.Reservations.Select(b => b.Spectactor)).
                Where(a => a.Spectacle.Id == spectacleId);
        }

        public IEnumerable<SeanceView> GetPage(int page, int pageSize, Guid? spectacleId, DateTime? date, out int total)
        {
            var pageParameter = new SqlParameter("@page", page);
            var pageSizeParameter = new SqlParameter("@pageSize", pageSize);
            var spectacleParameter = new SqlParameter("@spectacle", SqlDbType.UniqueIdentifier){Value = spectacleId.HasValue ? (object) spectacleId.Value : DBNull.Value};
            var dateParameter = new SqlParameter("@date", SqlDbType.DateTime) { Value = date.HasValue ? (object) date.Value : DBNull.Value};
            var totalParameter = new SqlParameter("@total", SqlDbType.Int) {Direction = ParameterDirection.Output};
            var list = 
                mContext.Database.SqlQuery<SeanceView>("GetSeancesPage @page, @pageSize, @spectacle, @date, @total OUTPUT", pageParameter, pageSizeParameter, spectacleParameter, dateParameter, totalParameter).ToList();
            total = (int) totalParameter.Value;
            return list;
        }

        public IEnumerable<Seance> GetAll()
        {
            return mContext.Seances.
                Include(a => a.Spectacle).
                Include(a => a.Reservations).
                Include(a => a.Reservations.Select(b => b.Spectactor)).
                OrderBy(a => a.Date);
        }

        public Seance GetById(Guid id)
        {
            return mContext.Seances.
                Include(a => a.Spectacle).
                Include(a => a.Reservations).
                Include(a => a.Reservations.Select(b => b.Spectactor)).
                FirstOrDefault(a => a.Id == id);
        }

        public void Add(Seance seance)
        {
            seance.Id = Guid.NewGuid();
            mContext.Seances.Add(seance);
        }

        public void Update(Seance seance)
        {
            var dbEntity = mContext.Seances.Find(seance.Id);
            if (dbEntity != null)
                mContext.Entry(dbEntity).CurrentValues.SetValues(seance);
        }

        public void Remove(Guid id)
        {
            var dbEntity = mContext.Seances.Find(id);
            if (dbEntity != null)
                mContext.Seances.Remove(dbEntity);
        }
    }
}
