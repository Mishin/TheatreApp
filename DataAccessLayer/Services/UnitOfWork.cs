﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.EF;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TheatreAppContext mContext;

        public UnitOfWork()
        {
            mContext = new TheatreAppContext();
            Seances = new SeanceRepository(mContext);
            Spectacles = new SpectacleRepository(mContext);
            Users = new UsersRepository(mContext);
        }

        public ISeanceRepository Seances { get; private set; }
        public ISpectacleRepository Spectacles { get; private set; }
        public IUserRepository Users { get; private set; }

        public void SaveChanges()
        {
            mContext.SaveChanges();
        }

        public void Dispose()
        {
            mContext.Dispose();
        }
    }
}
