﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using DataAccessLayer.EF;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System.Data.Entity;

namespace DataAccessLayer.Services
{
    public class UsersRepository : IUserRepository
    {
        private readonly TheatreAppContext mContext;

        public UsersRepository(TheatreAppContext context)
        {
            mContext = context;
        }

        public User GetByAuth(string login, string password)
        {
            var user = mContext.Users.FirstOrDefault(
                    a => a.Login.ToUpper() == login.ToUpper());
            return user != null && Crypto.VerifyHashedPassword(user.Password, password) ? 
                user : null;
        }

        public User GetById(Guid id)
        {
            return mContext.Users.Find(id);
        }

        public void Add(User user)
        {
            user.Id = Guid.NewGuid();
            mContext.Users.Add(user);
        }

        public void Update(User user)
        {
            var dbEntity = mContext.Users.Find(user.Id);
            if (dbEntity != null)
                mContext.Entry(dbEntity).CurrentValues.SetValues(user);
        }

        public User GetByUserName(string login)
        {
            return mContext.Users.Include(a => a.Reservations).FirstOrDefault(a => a.Login == login);
        }
    }
}
