﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using DataAccessLayer.Entities;

namespace DataAccessLayer.EF
{
    public class TheatreAppInitializer : CreateDatabaseIfNotExists<TheatreAppContext>
    {
        protected override void Seed(TheatreAppContext context)
        {
            ExecuteScript(context, "GetSeancesPage.sql");


            var adminUser = new User()
            {
                Id = Guid.NewGuid(),
                Email = "metrafortheatreapp@gmail.com",
                IsAdmin = true,
                Login = "admin",
                Password = Crypto.HashPassword("12345"),
                FirstName = "admin",
                LastName = "admin",
                CreationDate = DateTime.UtcNow
            };
            context.Users.Add(adminUser);

            base.Seed(context);
        }

        private void ExecuteScript(TheatreAppContext context, string scriptName)
        {
            string script = GetScript(scriptName);
            if (!string.IsNullOrWhiteSpace(script))
                context.Database.ExecuteSqlCommand(script);
        }

        private string GetScript(string fileName)
        {
            string script = string.Empty;

            try
            {
                var assembly = GetType().Assembly;
                var stream =
                    assembly.GetManifestResourceStream(assembly.GetName().Name + ".Scripts." + fileName);
                using (var sr = new StreamReader(stream))
                {
                    script = sr.ReadToEnd();
                }
            }
            catch
            {
            }
            return script;
        }
    }
}
