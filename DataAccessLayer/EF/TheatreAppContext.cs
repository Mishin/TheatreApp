﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace DataAccessLayer.EF
{
    public partial class TheatreAppContext : DbContext
    {
        public TheatreAppContext() : base("DefaultConnection") { }

        public DbSet<Spectacle> Spectacles { get; set; }

        public DbSet<Seance> Seances { get; set; }

        public DbSet<User> Users { get; set; }
    }
}
