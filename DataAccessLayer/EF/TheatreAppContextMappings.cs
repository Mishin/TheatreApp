﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace DataAccessLayer.EF
{
    public partial class TheatreAppContext
    {
        static TheatreAppContext()
        {
            Database.SetInitializer(new TheatreAppInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Spectacle>().
                HasMany(a => a.Seances).
                WithRequired(a => a.Spectacle).HasForeignKey(a => a.SpectacleId);

            modelBuilder.Entity<Reservation>().HasKey(a => new {a.SeanceId, a.SpectactorId});

            modelBuilder.Entity<Seance>().HasMany(a => a.Reservations).WithRequired(a => a.Seance).HasForeignKey(a => a.SeanceId);
            modelBuilder.Entity<User>().HasMany(a => a.Reservations).WithRequired(a => a.Spectactor).HasForeignKey(a => a.SpectactorId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
