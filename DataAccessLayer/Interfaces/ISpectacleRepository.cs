﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Interfaces
{
    public interface ISpectacleRepository
    {
        int GetTotalItemsCount();
        IEnumerable<Spectacle> GetAll();
        void Add(Spectacle spectacle);
        void Update(Spectacle spectacle);
        void Remove(Guid id);
        Spectacle GetById(Guid id);
    }
}
