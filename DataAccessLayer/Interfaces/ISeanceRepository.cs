﻿using System;
using System.Collections.Generic;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Interfaces
{
    public interface ISeanceRepository
    {
        IEnumerable<Seance> GetSeancesBySpectacle(Guid spectacleId);
        Seance GetById(Guid id);
        void Add(Seance seance);
        void Update(Seance seance);
        void Remove(Guid id);
        IEnumerable<SeanceView> GetPage(int page, int pageSize, Guid? spectacleId, DateTime? date, out int total);
        IEnumerable<Seance> GetAll();
    }
}
