﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Interfaces
{
    public interface IUserRepository
    {
        User GetByAuth(string login, string password);

        User GetById(Guid id);

        void Add(User user);

        void Update(User user);
        User GetByUserName(string login);
    }
}
