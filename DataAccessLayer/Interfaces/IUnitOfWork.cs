﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ISeanceRepository Seances { get; }
        ISpectacleRepository Spectacles { get; }
        IUserRepository Users { get; }

        void SaveChanges();
    }
}
