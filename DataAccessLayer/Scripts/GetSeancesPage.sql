﻿CREATE PROCEDURE GetSeancesPage
	@page int = 0,
	@pageSize int = 10,
	@spectacle uniqueidentifier = null,
	@date datetime = null,
	@total int OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET @total = (SELECT COUNT(*) FROM Seances s 
					WHERE 
				(@spectacle IS NULL OR s.SpectacleId = @spectacle) AND
				(@date IS NULL OR s.Date >= @date AND s.Date < DATEADD(DAY, 1, @date)))

	SELECT s.Id, s.Date, s.TicketsCount, s.SpectacleId, sp.Name AS SpectacleName, COALESCE(SUM(r.SeatsCount),0) AS ReservedSeatsCount FROM Seances s
	LEFT OUTER JOIN Reservations r ON s.Id = r.SeanceId
	INNER JOIN Spectacles sp ON s.SpectacleId = sp.Id
	WHERE 
	(@spectacle IS NULL OR s.SpectacleId = @spectacle) AND
	(@date IS NULL OR s.Date >= @date AND s.Date < DATEADD(DAY, 1, @date))
	GROUP BY s.Id, s.Date, s.TicketsCount, s.SpectacleId, sp.Name
	ORDER BY s.Date
	OFFSET @page * @pageSize ROWS FETCH NEXT @pageSize ROWS ONLY
END

