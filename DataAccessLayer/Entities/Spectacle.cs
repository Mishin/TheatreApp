﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class Spectacle
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public ICollection<Seance> Seances { get; set; }
    }
}
