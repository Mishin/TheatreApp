﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class Reservation
    {
        public Guid SpectactorId { get; set; }

        public Guid SeanceId { get; set; }

        public User Spectactor { get; set; }

        public Seance Seance { get; set; }

        public int SeatsCount { get; set; }
    }
}
