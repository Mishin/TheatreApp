﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class SeanceView
    {
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        public int TicketsCount { get; set; }

        public Guid SpectacleId { get; set; }

        public string SpectacleName { get; set; }

        public int ReservedSeatsCount { get; set; }
    }
}
