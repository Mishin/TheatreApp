﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class Seance
    {
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        public int TicketsCount { get; set; }

        public Guid SpectacleId { get; set; }
        public Spectacle Spectacle { get; set; }

        public ICollection<Reservation> Reservations { get; set; }
    }
}
