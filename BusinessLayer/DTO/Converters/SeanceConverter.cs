﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace BusinessLayer.DTO.Converters
{
    public static class SeanceConverter
    {
        public static SeanceDTO Convert(this Seance source)
        {
            return new SeanceDTO()
            {
                Id = source.Id,
                TicketsCount = source.TicketsCount,
                SpectacleId = source.SpectacleId,
                SpectacleName = source.Spectacle.Name,
                ReservedSeatsCount = source.Reservations.Select(a => a.SeatsCount).Sum(),
                Date = source.Date
            };
        }

        public static Seance Convert(this SeanceDTO source)
        {
            return new Seance
            {
                Id = source.Id,
                Date = source.Date,
                TicketsCount = source.TicketsCount,
                SpectacleId = source.SpectacleId
            };
        }

        public static SeanceDTO Convert(this SeanceView source)
        {
            return new SeanceDTO()
            {
                Id = source.Id,
                Date = source.Date,
                TicketsCount = source.TicketsCount,
                ReservedSeatsCount = source.ReservedSeatsCount,
                SpectacleId = source.SpectacleId,
                SpectacleName = source.SpectacleName
            };
        }
    }
}
