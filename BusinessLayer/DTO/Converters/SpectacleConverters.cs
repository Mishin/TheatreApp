﻿using DataAccessLayer.Entities;

namespace BusinessLayer.DTO.Converters
{
    public static class SpectacleConverters
    {
        public static SpectacleDTO Convert(this Spectacle source)
        {
            return new SpectacleDTO()
            {
                Id = source.Id,
                Name = source.Name
            };
        }

        public static Spectacle Convert(this SpectacleDTO source)
        {
            return new Spectacle
            {
                Id = source.Id,
                Name = source.Name
            };
        }
    }
}
