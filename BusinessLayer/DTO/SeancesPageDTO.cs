﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace BusinessLayer.DTO
{
    public class SeancesPageDTO
    {
        public IEnumerable<SeanceDTO> Page { get; set; }
        public int Total { get; set; }
    }
}
