﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataAccessLayer.Entities;

namespace BusinessLayer.DTO
{
    public class SeanceDTO
    {
        public SeanceDTO() : this(false)
        {
            
        }

        public SeanceDTO(bool isNew)
        {
            if (!isNew) return;
            var now = DateTime.Now;
            Date = now.Date.AddHours(now.Hour);
            TicketsCount = 50;
        }

        [HiddenInput(DisplayValue = false)]
        public Guid Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid SpectacleId { get; set; }

        [Display(Name = "Спектакль")]
        [HiddenInput]
        public string SpectacleName { get; set; }

        [Display(Name = "Количество билетов")]
        [Required(ErrorMessage = "Введите количество билетов")]
        [Range(0, Double.MaxValue, ErrorMessage = "Отрицательное количество билетов")]
        public int TicketsCount { get; set; }

        [Display(Name = "Количество забронированных мест")]
        [HiddenInput]
        public int ReservedSeatsCount { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0: DD.MM.yyyy hh:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата и время")]
        [Required(ErrorMessage = "Введите корректную дату")]
        [Range(typeof(DateTime), "01.01.1753", "01.01.3000", ErrorMessage = "Значение должно попадать в диапазон: (01.01.1753 - 01.01.3000)")]
        public DateTime Date { get; set; }

    }
}
