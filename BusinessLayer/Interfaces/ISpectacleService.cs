﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.DTO;

namespace BusinessLayer.Interfaces
{
    public interface ISpectacleService : IDisposable
    {
        int GetTotalItemsCount();

        IEnumerable<SpectacleDTO> GetAll();

        SpectacleDTO GetById(Guid id);

        void Add(SpectacleDTO spectacle);

        void Update(SpectacleDTO spectacle);

        void Remove(Guid id);
    }
}
