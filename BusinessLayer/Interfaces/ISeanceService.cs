using System;
using System.Collections.Generic;
using BusinessLayer.DTO;

namespace BusinessLayer.Interfaces
{
    public interface ISeanceService : IDisposable
    {
        IEnumerable<SeanceDTO> GetSeancesBySpectacle(Guid spectacleId);

        SeanceDTO GetById(Guid id);

        void Add(SeanceDTO seance);

        void Update(SeanceDTO seance);
        void Remove(Guid id);

        void ReserveUserToSeance(Guid seanceId, string login, int seatsCount);
        SeancesPageDTO GetPage(int page, int pageSize, Guid? spectacleId, DateTime? date);

        IEnumerable<SeanceDTO> GetAll();
    }
}