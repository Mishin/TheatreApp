﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace BusinessLayer.Interfaces
{
    public interface IUserService : IDisposable
    {
        User GetUserByLogin(string login);
        User Login(string login, string password);
        void Register(User user);
    }
}
