﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;

namespace BusinessLayer.Interfaces
{
    public interface ISeanceChangedNotificationService : IDisposable
    {
        void NotificateOnUpdate(Guid seanceId, DateTime newDate);
        void NotificateOnRemove(Guid seanceId);
    }
}
