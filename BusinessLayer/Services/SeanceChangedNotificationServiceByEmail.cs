﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using BusinessLayer.Interfaces;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;

namespace BusinessLayer.Services
{
    public class SeanceChangedNotificationServiceByEmail : ISeanceChangedNotificationService
    {
        private readonly IUnitOfWork mContext;

        public SeanceChangedNotificationServiceByEmail(IUnitOfWork context)
        {
            mContext = context;
        }

        public void NotificateOnUpdate(Guid seanceId, DateTime newDate)
        {
            var seance = mContext.Seances.GetById(seanceId);
            if (seance == null) return;

            if (seance.Date == newDate) return;
            if (!seance.Reservations.Any()) return;

            foreach (var reservation in seance.Reservations)
            {
                var body = string.Format(@"<p>Уважаемый {3}</p>
                                            <p>Сеанс на спектакль {0} ({1:f}) изменился.</p>
                                            <p>Новая дата и время: {2:f}.</p>", 
                                            seance.Spectacle.Name, seance.Date, newDate, GetSpectatorName(reservation.Spectactor));
                var res = reservation;
                Task.Run(()=> SendEmail(body, res.Spectactor.Email));

            }
        }

        public void NotificateOnRemove(Guid seanceId)
        {
            var seance = mContext.Seances.GetById(seanceId);
            if (seance == null) return;

            if (!seance.Reservations.Any()) return;
            foreach (var reservation in seance.Reservations)
            {
                var body = string.Format(@"<p>Уважаемый {2}</p>
                                            <p>Сеанс на спектакль {0} ({1:f}) был отменен.</p>
                                            <p>Приносим вам наши извенения.</p>",
                                            seance.Spectacle.Name, seance.Date, GetSpectatorName(reservation.Spectactor));
                var res = reservation;
                Task.Run(() => SendEmail(body, res.Spectactor.Email));

            }
        }


        private void SendEmail(string body, string email)
        {
            var message = new MailMessage();
            message.To.Add(new MailAddress(email));
            message.Subject = "Изменение расписания";
            message.Body = body;
            message.IsBodyHtml = true;
            using (var smtp = new SmtpClient())
            {
                try
                {
                    smtp.Send(message);
                }
                catch (Exception ex)
                {

                }
            }  
        }

        private string GetSpectatorName(User spectator)
        {
            if (string.IsNullOrWhiteSpace(spectator.FirstName) && string.IsNullOrWhiteSpace(spectator.LastName))
                return spectator.Login;
            return string.Format("{0} {1}", spectator.LastName, spectator.FirstName);
        }

        public void Dispose()
        {
            mContext.Dispose();
        }
    }
}
