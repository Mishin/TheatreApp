﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.DTO;
using BusinessLayer.DTO.Converters;
using BusinessLayer.Interfaces;
using DataAccessLayer.EF;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;

namespace BusinessLayer.Services
{
    public class SpectacleService : ISpectacleService
    {
        private readonly IUnitOfWork mContext;

        public SpectacleService(IUnitOfWork context)
        {
            mContext = context;
        }

        public int GetTotalItemsCount()
        {
            return mContext.Spectacles.GetTotalItemsCount();
        }

        public IEnumerable<SpectacleDTO> GetAll()
        {
            return mContext.Spectacles.GetAll().Select(a => a.Convert());
        }

        public SpectacleDTO GetById(Guid id)
        {
            var spectacle = mContext.Spectacles.GetById(id);
            if (spectacle == null) return null;
            return spectacle.Convert();
        }

        public void Add(SpectacleDTO spectacle)
        {
            mContext.Spectacles.Add(spectacle.Convert());
            mContext.SaveChanges();
        }

        public void Update(SpectacleDTO spectacle)
        {
            mContext.Spectacles.Update(spectacle.Convert());
            mContext.SaveChanges();
        }

        public void Remove(Guid id)
        {
            mContext.Spectacles.Remove(id);
            mContext.SaveChanges();
        }

        public void Dispose()
        {
            mContext.Dispose();
        }
    }
}
