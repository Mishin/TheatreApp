﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Interfaces;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;

namespace BusinessLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork mContext;

        public UserService(IUnitOfWork context)
        {
            mContext = context;
        }


        public User Login(string login, string password)
        {
            return mContext.Users.GetByAuth(login, password);
        }

        public User GetUserByLogin(string login)
        {
            return mContext.Users.GetByUserName(login);
        }

        public void Register(User user)
        {
            if (mContext.Users.GetByAuth(user.Login, user.Password) != null)
                throw new Exception("Пользователь с такими данными уже зарегистрирован");
            user.Id = Guid.NewGuid();
            mContext.Users.Add(user);
            mContext.SaveChanges();
        }

        public void Dispose()
        {
            mContext.Dispose();
        }
    }
}
