﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.DTO;
using BusinessLayer.DTO.Converters;
using BusinessLayer.Interfaces;
using DataAccessLayer.EF;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;

namespace BusinessLayer.Services
{
    public class SeanceService : ISeanceService
    {
        private readonly IUnitOfWork mContext;

        public SeanceService(IUnitOfWork context)
        {
            mContext = context;
        }

        public IEnumerable<SeanceDTO> GetSeancesBySpectacle(Guid spectacleId)
        {
            return mContext.Seances.GetSeancesBySpectacle(spectacleId).Select(a => a.Convert());
        }

        public SeancesPageDTO GetPage(int page, int pageSize, Guid? spectacleId, DateTime? date)
        {
            int total;
            var seances = mContext.Seances.GetPage(page, pageSize, spectacleId, date, out total);
            return new SeancesPageDTO(){Page = seances.Select(a => a.Convert()), Total = total};
        }

        public IEnumerable<SeanceDTO> GetAll()
        {
            return mContext.Seances.GetAll().Select(a => a.Convert());
        }


        public SeanceDTO GetById(Guid id)
        {
            var seance = mContext.Seances.GetById(id);
            return seance != null ? seance.Convert() : null;
        }

        public void Add(SeanceDTO seance)
        {
            mContext.Seances.Add(seance.Convert());
            mContext.SaveChanges();
        }

        public void Update(SeanceDTO seance)
        {
            mContext.Seances.Update(seance.Convert());
            mContext.SaveChanges();
        }

        public void Remove(Guid id)
        {
            mContext.Seances.Remove(id);
            mContext.SaveChanges();
        }

        public void ReserveUserToSeance(Guid seanceId, string login, int seatsCount)
        {
            var user = mContext.Users.GetByUserName(login);
            var seance = mContext.Seances.GetById(seanceId);
            if (user == null || seance == null) return;

            var countsList = seance.Reservations.Select(a => a.SeatsCount).ToList();
            var totalSeatsCount = countsList.Sum();

            if (totalSeatsCount + seatsCount > seance.TicketsCount)
                throw new Exception(string.Format("Оставшегося количества билетов не хватает для бронирования {0} мест", seatsCount));

            var reservation = seance.Reservations.FirstOrDefault(a => a.Spectactor.Id == user.Id);
            if (reservation == null)
            {
                reservation = new Reservation() {SeanceId = seance.Id, SpectactorId = user.Id, SeatsCount = seatsCount};
                seance.Reservations.Add(reservation);
            }
            else
                reservation.SeatsCount += seatsCount;

            mContext.SaveChanges();
        }

        public void Dispose()
        {
            mContext.Dispose();
        }
    }
}
