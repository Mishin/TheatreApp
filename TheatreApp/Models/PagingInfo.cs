﻿namespace TheatreApp.Models
{
    public class PagingInfo
    {
        public PagingInfo(int currentPage, int totalPagesCount)
        {
            CurrentPage = currentPage;
            TotalPagesCount = totalPagesCount;
        }


        public int CurrentPage { get; set; }

        public int TotalPagesCount { get; set; }
    }
}