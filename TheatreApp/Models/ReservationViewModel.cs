﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer.DTO;

namespace TheatreApp.Models
{
    public class ReservationViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public Guid SeanceId { get; set; }

        [Display(Name = "Количество мест")]
        [Required(ErrorMessage = "Введите количество мест")]
        [Range(1, Double.MaxValue, ErrorMessage = "Количество мест должно быть больше нуля")]
        public int SeatsCount { get; set; }
    }
}