﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLayer.DTO;

namespace TheatreApp.Models
{
    public class AfficheViewModel
    {
        public AfficheViewModel(IEnumerable<SeanceDTO> seances, int page, int totalPagesCount)
        {
            Seances = seances;
            PagingInfo = new PagingInfo(page, totalPagesCount);
        }

        public IEnumerable<SeanceDTO> Seances { get; private set; }

        public PagingInfo PagingInfo { get; private set; }
    }
}