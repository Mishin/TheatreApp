﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using DataAccessLayer.Entities;

namespace TheatreApp.Models.Security
{
    public class RegisterViewModel
    {
        [DisplayName("Логин")]
        [Required(ErrorMessage = "Введите логин")]
        public string Login { get; set; }

        [DisplayName("Имя")]
        [Required(ErrorMessage = "Введите имя")]
        public string FirstName { get; set; }

        [DisplayName("Фамилия")]
        [Required(ErrorMessage = "Введите фамилию")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Введите адрес электронной почты")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Неккоретный EMail")]
        public string Email { get; set; }

        [DisplayName("Пароль")]
        [Required(ErrorMessage = "Введите пароль")]
        public string Password { get; set; }

        [DisplayName("Подтвердить пароль")]
        [Required(ErrorMessage = "Введите подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают.")]
        public string ConfirmPassword { get; set; }


        public static explicit operator User(RegisterViewModel vm)
        {
            if (vm == null) return null;
            return new User()
            {
                Login = vm.Login,
                Password = vm.Password,
                FirstName = vm.FirstName,
                LastName = vm.LastName,
                Email = vm.Email,
            };
        }
    }
}