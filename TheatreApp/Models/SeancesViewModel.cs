﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer.DTO;

namespace TheatreApp.Models
{
    public class SeancesViewModel
    {
        [Display(Name = "Спектакль")]
        public string SpectacleName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid SpectacleId { get; set; }

        public IEnumerable<SeanceDTO> Seances { get; set; }
    }
}