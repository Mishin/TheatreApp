﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using BusinessLayer.Interfaces;
using DataAccessLayer.Entities;
using TheatreApp.Models;

namespace TheatreApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class HomeController : Controller
    {
        private readonly ISpectacleService mSpectacleService;

        public HomeController(ISpectacleService spectacleService)
        {
            mSpectacleService = spectacleService;
        }

        // GET: Admin/Home
        public ActionResult Index()
        {

            var spectacles = mSpectacleService.GetAll();
            return View(spectacles);
        }

        protected override void Dispose(bool disposing)
        {
            mSpectacleService.Dispose();
            base.Dispose(disposing);
        }
    }
}