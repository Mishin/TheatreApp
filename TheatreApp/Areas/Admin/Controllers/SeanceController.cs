﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer.DTO;
using BusinessLayer.Interfaces;

namespace TheatreApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class SeanceController : Controller
    {
        private readonly ISeanceService mSeanceService;
        private readonly ISpectacleService mSpectacleService;
        private readonly ISeanceChangedNotificationService mNotificationService;

        public SeanceController(ISeanceService seanceService, ISpectacleService spectacleService, ISeanceChangedNotificationService notificationService)
        {
            mSeanceService = seanceService;
            mSpectacleService = spectacleService;
            mNotificationService = notificationService;
        }

        public ActionResult Edit(Guid id)
        {
            var seance = mSeanceService.GetById(id);
            if (seance == null) HttpNotFound();
            return View(seance);
        }

        [HttpPost, ActionName("Edit")]
        public ActionResult EditConfirm(SeanceDTO seance)
        {
            if (ModelState.IsValid)
            {
                mNotificationService.NotificateOnUpdate(seance.Id, seance.Date);
                mSeanceService.Update(seance);
                return RedirectToAction("Seances", "Spectacle", new {id = seance.SpectacleId});
            }
            return View(seance);
        }

        public ActionResult Create(Guid spectacleId)
        {
            var spectacle = mSpectacleService.GetById(spectacleId);
            if (spectacle == null) return HttpNotFound();
            SeanceDTO dto = new SeanceDTO(true){SpectacleId = spectacleId, SpectacleName = spectacle.Name};
            return View(dto);
        }

        [HttpPost]
        public ActionResult Create(SeanceDTO seance)
        {
            if (ModelState.IsValid)
            {
                mSeanceService.Add(seance);
                return RedirectToAction("Seances", "Spectacle", new { id = seance.SpectacleId });
            }
            return View(seance);
        }

        public ActionResult Delete(Guid id)
        {

            var seance = mSeanceService.GetById(id);
            if (seance == null) HttpNotFound();
            return View(seance);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirm(Guid seanceId, Guid spectacleId)
        {
            mNotificationService.NotificateOnRemove(seanceId);
            mSeanceService.Remove(seanceId);
            return RedirectToAction("Seances", "Spectacle", new { id = spectacleId });
        }

        protected override void Dispose(bool disposing)
        {
            mSeanceService.Dispose();
            mSpectacleService.Dispose();
            mNotificationService.Dispose();
            base.Dispose(disposing);
        }
    }
}