﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer.DTO;
using BusinessLayer.Interfaces;
using TheatreApp.Models;

namespace TheatreApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class SpectacleController : Controller
    {
        private readonly ISeanceService mSeanceService;
        private readonly ISpectacleService mSpectacleService;
        private readonly ISeanceChangedNotificationService mNotificationService;

        public SpectacleController(ISeanceService seanceService, ISpectacleService spectacleService, ISeanceChangedNotificationService notificationService)
        {
            mSeanceService = seanceService;
            mSpectacleService = spectacleService;
            mNotificationService = notificationService;
        }

        public ActionResult Create()
        {
            return View(new SpectacleDTO());
        }

        [HttpPost]
        public ActionResult Create(SpectacleDTO spectacle)
        {
            if (ModelState.IsValid)
            {
                mSpectacleService.Add(spectacle);
                return RedirectToAction("Index", "Home");
            }
            return View(spectacle);
        }

        public ActionResult Edit(Guid id)
        {
            var spectacle = mSpectacleService.GetById(id);
            if (spectacle == null) return HttpNotFound();

            return View(spectacle);
        }

        [HttpPost]
        public ActionResult Edit(SpectacleDTO spectacle)
        {
            if (ModelState.IsValid)
            {
                mSpectacleService.Update(spectacle);
                return RedirectToAction("Index", "Home"); 
            }
            return View(spectacle);
        }

        public ActionResult Delete(Guid id)
        {
            var spectacle = mSpectacleService.GetById(id);
            if (spectacle == null) return HttpNotFound();

            return View(spectacle);

        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirm(Guid id)
        {
            var seances = mSeanceService.GetSeancesBySpectacle(id);
            foreach (var seance in seances)
                mNotificationService.NotificateOnRemove(seance.Id);
            mSpectacleService.Remove(id);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Seances(Guid id)
        {
            var spectacle = mSpectacleService.GetById(id);
            if (spectacle == null) return HttpNotFound();
            var seances = mSeanceService.GetSeancesBySpectacle(id);

            var vm = new SeancesViewModel()
            {
                SpectacleId = id,
                SpectacleName = spectacle.Name,
                Seances = seances
            };

            return View(vm);
        }

        protected override void Dispose(bool disposing)
        {
            mSeanceService.Dispose();
            mSpectacleService.Dispose();
            mNotificationService.Dispose();
            base.Dispose(disposing);
        }
    }
}