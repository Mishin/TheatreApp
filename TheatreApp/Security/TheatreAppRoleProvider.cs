﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using TheatreApp.Helpers;

namespace TheatreApp.Security
{
    public class TheatreAppRoleProvider : RoleProvider
    {
        public override bool IsUserInRole(string username, string roleName)
        {
            var user = UserServiceHelper.ProcessRequest(a => a.GetUserByLogin(username));
            return roleName == "admin" && user.IsAdmin;
        }

        public override string[] GetRolesForUser(string username)
        {
            var user = UserServiceHelper.ProcessRequest(a => a.GetUserByLogin(username));
            if (user == null)
                return new string[] {};
            return new[] {user.IsAdmin ? "admin" : "spectator"};
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName { get; set; }
    }
}