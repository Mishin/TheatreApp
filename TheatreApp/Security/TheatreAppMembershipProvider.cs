﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using BusinessLayer.Interfaces;
using DataAccessLayer.Entities;
using TheatreApp.Helpers;

namespace TheatreApp.Security
{
    public class TheatreAppMembershipProvider : MembershipProvider
    {


        public override bool ValidateUser(string username, string password)
        {
            var user = UserServiceHelper.ProcessRequest(a => a.Login(username, password));
            return user != null;
        }

        public MembershipUser CreateUser(string userName, string password, string email, string firstName, string lastName)
        {
            var membershipUser = GetUser(userName, false);
            if (membershipUser == null)
            {
                try
                {
                    var user = new User()
                    {
                        Login = userName,
                        Password = Crypto.HashPassword(password),
                        FirstName = firstName,
                        LastName = lastName,
                        Email = email,
                        CreationDate = DateTime.UtcNow,
                    };

                    UserServiceHelper.ProcessRequest(a => a.Register(user));
                    membershipUser = GetUser(userName, false);
                    return membershipUser;
                }
                catch
                {
                    return null;
                }
                
            }
            return null;
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            try
            {
                var user = UserServiceHelper.ProcessRequest(a => a.GetUserByLogin(username));
                return new MembershipUser(
                    "MyMembershipProvider",
                    user.Login,
                    null,
                    user.Email,
                    null,
                    null,
                    false,
                    false,
                    user.CreationDate,
                    DateTime.MinValue,
                    DateTime.MinValue,
                    DateTime.MinValue, 
                    DateTime.MinValue);
            }
            catch
            {
                return null;
            }
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer,
            bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion,
            string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        
        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        
        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override string ApplicationName { get; set; }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }
    }
}