﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer.Interfaces;

namespace TheatreApp.Helpers
{
    public static class UserServiceHelper
    {
        public static TResult ProcessRequest<TResult>(Func<IUserService, TResult> func)
        {
            var userService = (IUserService)DependencyResolver.Current.GetService(typeof(IUserService));
            TResult result;
            using (userService)
                result = func(userService);
            return result;
        }

        public static void ProcessRequest(Action<IUserService> action)
        {
            var userService = (IUserService)DependencyResolver.Current.GetService(typeof(IUserService));
            using (userService)
                action(userService);
        }

    }
}