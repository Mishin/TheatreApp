﻿using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using BusinessLayer.Interfaces;
using BusinessLayer.Services;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Services;

namespace TheatreApp
{
    public class AutofacConfig
    {
        public static void ConfigContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(typeof(MvcApplication).Assembly);
            
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
            builder.RegisterType<SeanceService>().As<ISeanceService>();
            builder.RegisterType<SpectacleService>().As<ISpectacleService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<SeanceChangedNotificationServiceByEmail>().As<ISeanceChangedNotificationService>();

            var container = builder.Build();

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}