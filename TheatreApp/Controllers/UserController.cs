﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BusinessLayer.Interfaces;
using DataAccessLayer.Entities;
using TheatreApp.Models;
using TheatreApp.Models.Security;
using TheatreApp.Security;

namespace TheatreApp.Controllers
{
    public class UserController : Controller
    {

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new LoginViewModel());
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginVm, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(loginVm.Login, loginVm.Password))
                {
                    FormsAuthentication.SetAuthCookie(loginVm.Login, true);
                    if (Url.IsLocalUrl(returnUrl))
                        return Redirect(returnUrl);
                    return RedirectToAction("Index", "Home");
                    
                }
                ModelState.AddModelError("", "Неправильный логин или пароль");
            }
            ViewBag.ReturnUrl = returnUrl;
            return View(loginVm);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel registerVm)
        {
            if (ModelState.IsValid)
            {
                MembershipUser membershipUser = ((TheatreAppMembershipProvider)Membership.Provider).
                    CreateUser(registerVm.Login, registerVm.Password, registerVm.Email, registerVm.FirstName, registerVm.LastName);
                if (membershipUser != null)
                {
                    FormsAuthentication.SetAuthCookie(registerVm.Login, false);
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Ошибка при регистрации.\nСкорее всего пользователь с такими данными уже существует.");
            }
            return View(registerVm);
        }
    }
}