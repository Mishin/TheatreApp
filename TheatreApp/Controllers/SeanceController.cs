﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLayer.Interfaces;
using TheatreApp.Models;

namespace TheatreApp.Controllers
{
    public class SeanceController : ApiController
    {
        private const int PageSize = 5;
        private readonly ISeanceService mSeanceService;

        public SeanceController(ISeanceService seanceService)
        {
            mSeanceService = seanceService;
        }

        [HttpGet]
        public AfficheViewModel Affiche(int? page, Guid? spectacleId, DateTime? date)
        {
            var pageDto = mSeanceService.GetPage(page.GetValueOrDefault(), PageSize, spectacleId, date);

            var vm = new AfficheViewModel(pageDto.Page, page.GetValueOrDefault(),
                (int)Math.Ceiling((decimal)pageDto.Total / PageSize));
            return vm;
        }
    }
}
