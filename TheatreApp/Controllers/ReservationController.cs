﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using BusinessLayer.DTO;
using BusinessLayer.Interfaces;
using TheatreApp.Models;

namespace TheatreApp.Controllers
{
//    [Authorize]
    public class ReservationController : ApiController
    {
        private readonly ISeanceService mSeanceService;

        public ReservationController(ISeanceService seanceService)
        {
            mSeanceService = seanceService;
        }

        [HttpPut]
        public HttpResponseMessage Reserve(ReservationViewModel reservation)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    mSeanceService.ReserveUserToSeance(reservation.SeanceId, User.Identity.Name, reservation.SeatsCount);
                    var seance = mSeanceService.GetById(reservation.SeanceId);
                    return Request.CreateResponse<dynamic>(HttpStatusCode.OK,
                        new
                        {
                            seanceId = reservation.SeanceId,
                            reservedSeatsCount = seance.ReservedSeatsCount
                        });

                }
                catch (Exception ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
                finally 
                {
                    mSeanceService.Dispose();
                }
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }
    }
}
