﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLayer.DTO;
using BusinessLayer.Interfaces;

namespace TheatreApp.Controllers
{
    public class SpectacleController : ApiController
    {
        private readonly ISpectacleService mSpectacleService;
        private readonly ISeanceService mSeanceService;

        public SpectacleController(ISpectacleService spectacleService, ISeanceService seanceService)
        {
            mSpectacleService = spectacleService;
            mSeanceService = seanceService;
        }

        [HttpGet]
        [ActionName("spectacles")]
        public IEnumerable<SpectacleDTO> Spectacles()
        {
            var spectacles = mSpectacleService.GetAll();
            return spectacles;
        }

        [HttpGet]
        [ActionName("dates")]
        public IEnumerable<DateTime> Dates(Guid? id)
        {
            IEnumerable<SeanceDTO> seances;
            seances = id.HasValue
                ? mSeanceService.GetSeancesBySpectacle(id.Value)
                : mSeanceService.GetAll();
            var dates = seances.Select(a => a.Date)
                .GroupBy(a => new { a.Year, a.Month, a.Day })
                .Select(a => new DateTime(a.Key.Year, a.Key.Month, a.Key.Day))
                .ToList();
            return dates;
        } 
    }
}
