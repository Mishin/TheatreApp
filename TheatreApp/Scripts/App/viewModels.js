﻿
urlHelper = {
    baseUrl: null,
    buildUrl: function(url)
    {
        return (this.baseUrl || '') + url + '/' 
    }
}

function PagingInfo() {
    var self = this;
    self.currentPage = ko.observable(0);
    self.totalPagesCount = ko.observable();

    self.pagesList = ko.computed(function () {
        var count = self.totalPagesCount();
        var res = [];
        for (var i = 0; i < count; i++) {
            res.push(i);
        }
        return res;
    });
}

function Spectacle(data) {
    var self = this;
    self.id = data.Id;
    self.name = data.Name;
}

function FilteringMenuViewModel() {
    var self = this;

    self.spectacles = ko.observableArray();
    self.selectedSpectacle = ko.observable(null);

    self.dates = ko.observableArray();
    self.selectedDate = ko.observable(null);

    ko.computed(function () {
        $.ajax({
            url: urlHelper.buildUrl('/api/spectacle/spectacles'),
            type: 'GET',
            success: function(data) {
                var res = [{ id: null, name: 'Все' }];
                $.each(data, function(index, value) {
                    res.push(new Spectacle(value));
                });
                self.spectacles(res);
            }
        });
    });

    ko.computed(function () {
        var spectacle = self.selectedSpectacle();
        var param = { id: spectacle };
        $.ajax({
            url: urlHelper.buildUrl('/api/spectacle/dates'),
            type: 'GET',
            data: param,
            success: function (data) {
                var res = [null];
                $.each(data, function (index, value) {
                    res.push(moment(value));
                });
                self.dates(res);
                self.selectedDate(null);
            }
        });
    });
}

function Seance(data) {
    var self = this;
    self.id = data.Id;
    self.spectacleId = data.SpectacleId;
    self.spectacleName = data.SpectacleName;
    self.ticketsCount = data.TicketsCount;
    self.reservedSeatsCount = ko.observable(data.ReservedSeatsCount);
    self.date = moment(data.Date);

    self.availableSeatsCount = ko.computed(function () {
        return self.ticketsCount - self.reservedSeatsCount();
    });
}

function ReserveViewModel(seance) {
    var self = this;
    self.seance = seance;
    self.seatsCount = ko.observable();
    self.isBusy = ko.observable(false).extend({ rateLimit: { timeout: 200 } });
    self.errors = ko.observableArray();
    self.reserve = function () {
        var params = { seanceId: self.seance.id, seatsCount: self.seatsCount() };
        $.ajax({
            url: urlHelper.buildUrl('/api/reservation'),
            type: 'PUT',
            data: params,
            success: function (data) {
                self.modal.close(data);
            },
            error: function (jxqr, error, status) {
                self.errors.removeAll();
                var response = $.parseJSON(jxqr.responseText);
                if (response.ModelState) {
                    $.each(response.ModelState['reservation.SeatsCount'],
                        function(index, value) {
                            self.errors.push(value);
                        });
                } else if (response.Message){
                    self.errors.push(response.Message);
                }
            }
        });
        
    };
    self.cancel = function () {
        self.modal.close();
    }
}

function AfficheViewModel() {
    var self = this;

    self.seances = ko.observableArray();

    self.pagingInfo = new PagingInfo();
    self.filteringMenuViewModel = new FilteringMenuViewModel();


    self.reserve = function (seance) {
            showModal({ viewModel: new ReserveViewModel(seance), template: 'reserve-modal', context: this }).
                then(function (data) {
                    var seances = self.seances();
                    for (var i = 0; i < seances.length; i++) {
                        var seance = seances[i];
                        if (seance.id == data.seanceId) {
                            seance.reservedSeatsCount(data.reservedSeatsCount);
                            break;
                        }
                    }
                });
    }

    function updatePagingInfo(pagingInfo) {
        var info = self.pagingInfo;
        info.currentPage(pagingInfo.CurrentPage);
        info.totalPagesCount(pagingInfo.TotalPagesCount);
    }

    function updateSeances(seances) {
        var res = [];
        $.each(seances, function (index, value) {
            res.push(new Seance(value));
        });
        self.seances(res);
    }

    self.isBusy = ko.observable(true).extend({ rateLimit: { timeout: 200 } });

    ko.computed(function () {
        self.filteringMenuViewModel.selectedSpectacle();
        self.filteringMenuViewModel.selectedDate();
        self.pagingInfo.currentPage(0);
    });

    ko.computed(function () {
        var selectedDate = self.filteringMenuViewModel.selectedDate();
        var params = {
            page: self.pagingInfo.currentPage(),
            spectacleId: self.filteringMenuViewModel.selectedSpectacle(),
            date: selectedDate ? selectedDate.toJSON() : null
        };
        self.isBusy(true);
        $.ajax({
            url: urlHelper.buildUrl('/api/seance'),
            type: 'GET',
            data: params,
            success: function (data) {
                updateSeances(data.Seances);
                updatePagingInfo(data.PagingInfo);
            },
            complete: function() {
                self.isBusy(false);
            }
        });
    });
};